var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    neat        = require('node-neat').includePaths,
    watch       = require('gulp-watch'),
    sassLint    = require('gulp-sass-lint'),
    plumber     = require('gulp-plumber'),
    gutil       = require('gulp-util'),
    livereload  = require('gulp-livereload'),
    jshint      = require('gulp-jshint'),
    sourcemaps  = require('gulp-sourcemaps'),
    filter      = require('gulp-filter'),

    input  = {
      'sass': './sass/**/*.scss',
      'javascript': './js/**/*.js',
    },

    output = {
      'stylesheets': './css',
      'javascript': './js'
    },

    livereloadFiles = {
      'stylesheets' : [
        'css/style.css'
      ]
    }

/**
* Compiles scss files to css files.
* Additional features:
*  - Generates Sourcemaps.
*  - Reloads the browser (injects css).
*  - Lints the scss files.
**/
gulp.task('sass-compile', function () {
  return gulp.src(input.sass)
    .pipe(plumber(function(error) {
      gutil.log(gutil.colors.red(error.message));
      this.emit('end');
    }))
    .pipe(sassLint())
    .pipe(sassLint.format())
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths: ['styles'].concat(neat)
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(output.stylesheets))
    .pipe(filter(livereloadFiles.stylesheets))
    .pipe(livereload());
});

/**
* Runs javascript through jshint.
**/
gulp.task('js-hint', function() {
  return gulp.src(input.javascript)
    .pipe(plumber(function(error) {
      gutil.log(gutil.colors.red(error.message));
      this.emit('end');
    }))
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(livereload());
});

/**
* Runs scss files trough a Sass Linter
**/
gulp.task('sass-lint', function () {
  return gulp.src(input.sass)
    .pipe(sassLint())
    .pipe(sassLint.format());
});

gulp.task('default',function () {
  gulp.start('sass-lint');
  gulp.start('sass-compile');
  gulp.start('js-hint');
});

gulp.task('watch', function () {
  livereload.listen();
  gulp.watch(input.sass, ['sass-compile']);
  gulp.watch(input.javascript, ['js-hint']);
});
