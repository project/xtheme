/**
 * @file
 * Custom JS for xTheme.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * Example behavior.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      console.log('xTheme is locked and loaded');
    }
  };

})(jQuery, Drupal);
